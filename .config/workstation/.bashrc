#!/usr/bin/env bash

# shellcheck disable=SC1090,SC1091,SC2139

# If not running interactively, don't do anything
[[ $- != *i* ]] && return 0

path_exists() {
    [ -d "$1" ]
}

merge_to_path() {
    [ $# -lt 1 ] && { echo 'Guy!, provide a path'; return; }
    realpath / > /dev/null 2>&1 && path=$(realpath "$1") || path="$1"

    if ! echo "$PATH" | /bin/grep -Eq "(^|:)$path($|:)";
    then
        if [ "$2" = "after" ];
        then
            PATH="$PATH:$path"
        else
            PATH="$path:$PATH"
        fi
    fi
}

\. "${HOME}/.config/workstation/.environment"

unset merge_to_path path_exists

# SSH
if [ -n "${WKSTN_CUSTOM_SETUP:-}" ]; then
  [ -r "${HOME}/.ssh/ssh-agent-env" ] && \. "${HOME}/.ssh/ssh-agent-env" &> /dev/null
fi

# Enable tab completion when starting a command with 'runas'
# [ "$PS1" ] && complete -cf runas

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Append to the Bash history file, rather than overwriting it
shopt -s histappend

# Autocorrect typos in path names when using `cd`
shopt -s cdspell

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
  shopt -s "$option" 2> /dev/null
done

# Enable automatic color support for common commands that list output
# and also add handy aliases.  Note the link to the `dircolors`.
if command -v dircolors > /dev/null; then
  dircolors_definitions="${XDG_DATA_HOME}/colours/dircolors"
  if [ -r "$dircolors_definitions" ];
  then
    eval "$(dircolors -b "${dircolors_definitions}")"
  else
    eval "$(dircolors -b)"
  fi
  unset dircolors_definitions
fi

# Add tab completion for SSH hostnames based on ~/.ssh/config
# ignoring wildcards
[[ -e "$HOME/.ssh/config" ]] && complete -o "default" \
  -o "nospace" \
  -W "$(grep "^Host" ~/.ssh/config | \
  grep -v "[?*]" | cut -d " " -f2 | \
  tr ' ' '\n')" scp sftp ssh

# Make `less` more friendly for non-text input files.
command -v lesspipe > /dev/null && eval "$(SHELL=/bin/sh lesspipe)"

# == Extensions
# nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# shellcheck disable=SC1090
# Setup fzf
# ---------
if command -v fzf > /dev/null; then
  # Auto-completion
  [ -r /usr/share/bash-completion/completions/fzf ] && \
    \. /usr/share/bash-completion/completions/fzf

  # Key bindings
  # ------------
  [ -r /usr/share/doc/fzf/key-bindings.bash ] && \
    \. /usr/share/doc/fzf/key-bindings.bash
fi
