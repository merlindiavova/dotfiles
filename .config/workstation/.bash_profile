#!/usr/bin/env bash

# shellcheck source=/dev/null
[[ -r "${HOME}/.bashrc" ]] && \. "${HOME}/.bashrc"
