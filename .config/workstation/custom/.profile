# shellcheck disable=SC1090,SC1091,SC2148

[ -n "${BASH_VERSION:-}" ] && [ -r "${HOME}/.bashrc" ] && \. "${HOME}/.bashrc"

export XDG_DATA_HOME=${XDG_DATA_HOME:="${HOME}/.local/share"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="${HOME}/.config"}
export XDG_DATA_DIRS=${XDG_DATA_DIRS:=/usr/local/share/:/usr/share/}
export XDG_CONFIG_DIRS=${XDG_CONFIG_DIRS:=/etc/xdg}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="${HOME}/.cache"}
export WKSTN_WM=${WKSTN_WM:="cwm"}

if [ -n "${XDG_RUNTIME_DIR:-}" ]; then
    export XDG_RUNTIME_DIR="/run/user/${USER_ID}"
    readonly XDG_RUNTIME_DIR
fi

export XDG_DOWNLOAD_DIR=${XDG_DOWNLOAD_DIR:="${HOME}/downloads"}
export XDG_MUSIC_DIR=${XDG_MUSIC_DIR:="${HOME}/music"}
export XDG_VIDEOS_DIR=${XDG_VIDEOS_DIR:="${HOME}/videos"}
export XDG_PICTURES_DIR=${XDG_PICTURES_DIR:="${HOME}/pictures"}

if [ "$(tty)" = "/dev/tty1" ]; then
    pgrep "$WKSTN_WM" || startx "$HOME/.xinitrc"
fi
