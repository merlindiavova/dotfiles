#!/usr/bin/env sh
# dotsgen - Concatenate my core dot files into .bashrc

set -eu

# To the extent possible under law, Merlin Diavova <email-address>
# has waived all copyright and related or neighboring rights to this work.
# http://creativecommons.org/publicdomain/zero/1.0/

DOTS_DIR=${DOTS_DIR:="${HOME}"}
DOTS_CONFIG_DIR=${DOTS_CONFIG_DIR:="${DOTS_DIR}/.config/workstation"}

main() {
    aliases="${DOTS_CONFIG_DIR}/.aliases"
    functions="${DOTS_CONFIG_DIR}/.functions"
    divider="${HOME}/s.tmp"

    if [ "${WKSTN_OS:-}" = 'Void' ]; then
        void_config="${DOTS_CONFIG_DIR}/void-linux"
        aliases="${aliases} ${void_config}/.aliases.xbps"
        functions="${functions} ${void_config}/.functions.xbps"
    fi

    if [ -r "${HOME}/.aliases.local" ]; then
        aliases="${aliases} ${HOME}/.aliases.local"
    fi

    if [ -r "${HOME}/.functions.local" ]; then
        functions="${functions} ${HOME}/.functions.local"
    fi

    echo '' > "$divider"

    files="${DOTS_CONFIG_DIR}/.bashrc ${divider} ${aliases} ${divider}  ${functions} ${divider}  ${DOTS_CONFIG_DIR}/.bash_prompt"

    # shellcheck disable=SC2086
    cat $files > "${HOME}/.bashrc"

    rm "$divider"
}

main "$@"
